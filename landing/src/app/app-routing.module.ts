import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponentComponent } from 'src/Pages/home-component/home-component.component';
import { SelectedCategoryComponent } from 'src/Pages/selected-category/selected-category.component';
import { CategoryTypeComponent } from 'src/Pages/category-type/category-type.component';
import { ViewProductComponent } from 'src/Pages/view-product/view-product.component';


const routes: Routes = [
  { path: 'Home', component: HomeComponentComponent },
  { path: '', component: HomeComponentComponent },
  { path: 'Category1', component: SelectedCategoryComponent },
  { path: 'Category1/Type', component: CategoryTypeComponent },
  { path: 'Product', component: ViewProductComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
