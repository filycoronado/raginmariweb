import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponentComponent } from 'src/Pages/home-component/home-component.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';  
import { CarouselComponent } from './components/carousel/carousel.component';
import { SelectedCategoryComponent } from 'src/Pages/selected-category/selected-category.component';
import { OwlModule } from 'ngx-owl-carousel';
import { OwCarouselComponent } from './components/ow-carousel/ow-carousel.component';
import { CategoryTypeComponent } from 'src/Pages/category-type/category-type.component';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import * as Hammer from 'hammerjs';
import { NgxGalleryModule } from 'ngx-gallery';
import { ViewProductComponent } from 'src/Pages/view-product/view-product.component';

export class CustomHammerConfig extends HammerGestureConfig {
  overrides = {
    'pan': {
      direction: Hammer.DIRECTION_ALL,
    }
  }
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponentComponent,
    CategoryTypeComponent,
    CarouselComponent,
    SelectedCategoryComponent,
    OwCarouselComponent,
    ViewProductComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,NgbModule ,OwlModule,NgxGalleryModule
  ],
  providers: [
    {provide: HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
