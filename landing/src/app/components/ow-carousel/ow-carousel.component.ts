import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ow-carousel',
  templateUrl: './ow-carousel.component.html',
  styleUrls: ['./ow-carousel.component.css']
})
export class OwCarouselComponent implements OnInit {

  title = 'owlcarouselinAngular';
  Images = ['./../../../assets/img/baughman-bar-cart-qv.png', './../../../assets/img/baughman-bar-cart-qv.png', './../../../assets/img/baughman-bar-cart-qv.png'];

  SlideOptions = { items: 3, dots: true, nav: true };
  CarouselOptions = { items: 3, dots: true, nav: true };
  constructor() { }

  ngOnInit() {
  }

}
