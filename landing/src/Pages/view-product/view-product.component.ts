import { Component, OnInit } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent implements OnInit {

  constructor() { }

  
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  ngOnInit(): void {

    this.galleryOptions = [
      {
        width: '600px',
        height: '400px',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];

    this.galleryImages = [
      {
        small: './assets/img/curtis-sofa-qv.png',
        medium: './assets/img/curtis-sofa-qv.png',
        big: './assets/img/curtis-sofa-qv.png'
      },
      {
        small: './assets/img/curtis-sofa-qv.png',
        medium: './assets/img/curtis-sofa-qv.png',
        big: './assets/img/curtis-sofa-qv.png'
      },
      {
        small: './assets/img/curtis-sofa-qv.png',
        medium: './assets/img/curtis-sofa-qv.png',
        big: './assets/img/curtis-sofa-qv.png'
      }
    ];
  }
}
