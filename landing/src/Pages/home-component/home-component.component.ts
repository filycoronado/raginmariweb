import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
@Component({
  selector: 'app-home-component',
  templateUrl: './home-component.component.html',
  styleUrls: ['./home-component.component.css']
})
export class HomeComponentComponent implements OnInit {
  policies: any = [
    { id: 0, name: "policy001" },
    { id: 2, name: "policy002" },
    { id: 3, name: "policy003" },
    { id: 4, name: "policy004" },
    { id: 5, name: "policy005" },
    { id: 6, name: "policy005" },
  ];
  constructor() { }

  ngOnInit() {
  }

}
